<?php
require "vendor/autoload.php";
$app = new \Slim\App();$app->get('/customers/{number}', function($request, $response,$args){
    $json = '{"1":"Eden", "2":"Nir", "3":"Yosef"}';
    $array = (json_decode($json, true));
    if(array_key_exists($args['number'], $array)){
        echo $array[$args['number']];
    }
    else{
        echo "Error User";
    }
});
$app->run();